# Joomla! package for installing the userpush plugin.

This repository only contains the installation files; for more information,
see the userpush extension repository:
https://gitlab.com/joomla-bits/userpush

Any problems would be reported in the common issue tracker of the userpush
extension, if it existed it would be located here:
https://gitlab.com/joomla-bits/userpush/issues
